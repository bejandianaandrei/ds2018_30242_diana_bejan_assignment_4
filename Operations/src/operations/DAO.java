package operations;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DAO {

    public List<Package> getPackages(int clientId, String filter) {
        try {
            Connection connection = DB.createConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM package WHERE (sender_id = " + clientId +
                    " OR receiver_id = " + clientId + ")";
            if (filter != null && !filter.equals("")) {
                sql += " AND name LIKE \"%" + filter + "%\"";
            }
            ResultSet rs = statement.executeQuery(sql);
            List<Package> packages = new ArrayList<>();
            while (rs.next()) {
                Package p = new Package();
                p.setId(rs.getInt("id"));
                p.setSenderId(rs.getInt("sender_id"));
                p.setReceiverId(rs.getInt("receiver_id"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));
                p.setSenderCity(rs.getString("sender_city"));
                p.setDestinationCity(rs.getString("destination_city"));
                packages.add(p);
            }
            rs.close();
            statement.close();
            connection.close();
            return packages;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<PackageUpdate> getPackageUpdates(int packageId) {
        try {
            Connection connection = DB.createConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM package_update WHERE package_id = " + packageId;
            ResultSet rs = statement.executeQuery(sql);
            List<PackageUpdate> packages = new ArrayList<>();
            while (rs.next()) {
                PackageUpdate p = new PackageUpdate();
                p.setCity(rs.getString("city"));
                p.setTime(rs.getDate("time"));
                packages.add(p);
            }
            rs.close();
            statement.close();
            connection.close();
            return packages;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
