package operations;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.List;

@WebService()
public class Service {

    @WebMethod
    public List<Package> getAllPackages(int userId) {
        return new DAO().getPackages(userId, null);
    }

    @WebMethod
    public List<Package> searchPackages(int userId, String filter) {
        return new DAO().getPackages(userId, filter);
    }

    @WebMethod
    public List<PackageUpdate> getPackageUpdates(int packageId) {
        return new DAO().getPackageUpdates(packageId);
    }

    public static void main(String[] args) {
        Object implementor = new Service();
        String address = "http://localhost:9000/Operations";
        Endpoint.publish(address, implementor);
    }
}
