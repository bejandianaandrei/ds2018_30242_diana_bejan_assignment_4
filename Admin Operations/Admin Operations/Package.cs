﻿namespace Admin_Operations
{
    public class Package
    {
        public int Id { get; set; }
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SenderCity { get; set; }
        public string DestinationCity { get; set; }
        public bool Tracking { get; set; }

        public Package()
        {
        }
    }
}