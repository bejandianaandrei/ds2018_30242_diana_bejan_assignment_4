﻿using System.Collections.Generic;
using System.Web.Services;

namespace Admin_Operations
{
    /// <summary>
    /// Summary description for AdminOperations
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AdminOperations : System.Web.Services.WebService
    {
        [WebMethod]
        public Client GetClient(string username)
        {
            return new ClientDAO().Get(username);
        }

        [WebMethod]
        public List<Client> GetClients()
        {
            return new ClientDAO().Get();
        }

        [WebMethod]
        public List<Package> GetPackages()
        {
            return new PackageDAO().Get();
        }

        [WebMethod]
        public void CreatePackage(Package package)
        {
            new PackageDAO().Insert(package);
        }

        [WebMethod]
        public void RegisterPackage(int packageId)
        {
            new PackageDAO().SetTracking(packageId, true);
        }

        [WebMethod]
        public void CreatePackageUpdate(PackageUpdate packageUpdate)
        {
            new PackageUpdateDAO().Insert(packageUpdate);
        }
    }
}
