﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Admin_Operations
{
    public class PackageDAO
    {
        public List<Package> Get()
        {
            string query = "SELECT * FROM package";
            var conn = DB.NewConnection();
            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            List<Package> packages = new List<Package>();
            while (dataReader.Read())
            {
                Package p = new Package
                {
                    Id = (int)dataReader["id"],
                    SenderId = (int)dataReader["sender_id"],
                    ReceiverId = (int)dataReader["receiver_id"],
                    Name = (string)dataReader["name"],
                    Description = (string)dataReader["description"],
                    SenderCity = (string)dataReader["sender_city"],
                    DestinationCity = (string)dataReader["destination_city"],
                    Tracking = (bool)dataReader["tracking"]
                };
                packages.Add(p);
            }
            dataReader.Close();
            conn.Close();
            return packages;
        }

        public void Insert(Package p)
        {
            string query = "INSERT INTO package(sender_id, receiver_id, name, description, sender_city, destination_city, tracking) VALUES (" +
                    p.SenderId + ", " +
                    p.ReceiverId + ", " +
                    "\"" + p.Name + "\", " +
                    "\"" + p.Description + "\", " +
                    "\"" + p.SenderCity + "\", " +
                    "\"" + p.DestinationCity + "\",0)";
            var conn = DB.NewConnection();
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void SetTracking(int id, bool val)
        {
            string query = "UPDATE package SET tracking = " + (val ? 1 : 0) + " WHERE id = " + id;
            var conn = DB.NewConnection();
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}