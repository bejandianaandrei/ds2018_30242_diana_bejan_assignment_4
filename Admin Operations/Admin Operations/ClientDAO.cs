﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Admin_Operations
{
    public class ClientDAO
    {
        public Client Get(string username)
        {
            string query = "SELECT * FROM client WHERE " +
                "username = \"" + username + "\"";
            var conn =  DB.NewConnection();
            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            Client client = null;
            if (dataReader.Read())
            {
                client = new Client
                {
                    Id = (int)dataReader["id"],
                    Username = (string)dataReader["username"],
                    Password = (string)dataReader["password"],
                    IsAdmin = (bool)dataReader["is_admin"]
                };
            }
            dataReader.Close();
            conn.Close();
            return client;
        }

        public List<Client> Get()
        {
            string query = "SELECT * FROM client";
            var conn = DB.NewConnection();
            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            List<Client> clients = new List<Client>();
            while (dataReader.Read())
            {
                Client client = new Client();
                client.Id = (int)dataReader["id"];
                client.Username = (string)dataReader["username"];
                client.Password = (string)dataReader["password"];
                client.IsAdmin = (bool)dataReader["is_admin"];
                clients.Add(client);
            }
            dataReader.Close();
            conn.Close();
            return clients;
        }
    }
}