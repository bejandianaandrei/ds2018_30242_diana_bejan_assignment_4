﻿using MySql.Data.MySqlClient;

namespace Admin_Operations
{
    public class PackageUpdateDAO
    {
        public void Insert(PackageUpdate pu)
        {
            string query = "INSERT INTO package_update (package_id, city, time) VALUES (" +
                    pu.PackageId + ", " +
                    "\"" + pu.City + "\", " +
                    "\"" + string.Format("{0}-{1}-{2} {3}:{4}:{5}", pu.Time.Year, pu.Time.Month, pu.Time.Day, pu.Time.Hour, pu.Time.Minute, pu.Time.Second) + "\")";
            var conn = DB.NewConnection();
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}