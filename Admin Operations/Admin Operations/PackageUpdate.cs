﻿namespace Admin_Operations
{
    public class PackageUpdate
    {
        public int Id { get; set; }
        public int PackageId { get; set; }
        public string City { get; set; }
        public MySql.Data.Types.MySqlDateTime Time { get; set; }

        public PackageUpdate()
        {
        }
    }
}