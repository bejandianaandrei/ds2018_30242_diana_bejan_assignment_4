﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using online_tracking_system.ClientReference;

namespace online_tracking_system
{
    public partial class Form3 : Form
    {
        private ServiceClient clientOps;
        private int clientId;
        private List<package> packages;

        public Form3(ServiceClient clientOps, int clientId)
        {
            InitializeComponent();
            this.clientOps = clientOps;
            this.clientId = clientId;
            package[] packagesArray = clientOps.getAllPackages(clientId);
            packages = packagesArray != null ? packagesArray.ToList() : new List<package>();
            DisplayPackages();
        }

        private void DisplayPackages()
        {
            listBox1.Items.Clear();
            listBox1.Items.AddRange(
                packages.FindAll(
                    p => textBox1.Text == ""
                        ? true
                        : p.name.ToLower().Contains(
                            textBox1.Text.ToLower()
                    )
                ).Select(p => p.name).ToArray()
            );
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DisplayPackages();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
            {
                return;
            }
            package p = packages.Find(package => package.name == (string)listBox1.SelectedItem);
            textBox2.Text = "Sender Id: " + p.senderId + "\r\nReceiver Id: " + p.receiverId + "\r\nDescription: " + p.description + "\r\nFrom: " + p.senderCity + "\r\nTo: " + p.destinationCity;
            textBox3.Text = "";
            packageUpdate[] packageUpdatesArray = clientOps.getPackageUpdates(p.id);
            List<packageUpdate> packageUpdates = packageUpdatesArray != null ? packageUpdatesArray.ToList() : new List<packageUpdate>();
            packageUpdates.Select(u => u.city + ", " + u.timeString).ToList().ForEach(s => textBox3.Text += s + "\n");
        }
    }
}
