﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using online_tracking_system.AdminServiceReference;

namespace online_tracking_system
{
    public partial class Form2 : Form
    {
        private AdminOperationsSoapClient adminOps;
        private List<Client> clients;
        private List<Package> packages;

        public Form2(AdminOperationsSoapClient adminOps)
        {
            InitializeComponent();
            this.adminOps = adminOps;
            Client[] clientsArray = adminOps.GetClients();
            clients = clientsArray != null ? clientsArray.ToList() : new List<Client>();
            string[] clientNames = clients.Select(c => c.Username).ToArray();
            comboBox1.Items.AddRange(clientNames);
            comboBox2.Items.AddRange(clientNames);
            GetPackages();
        }

        private void GetPackages()
        {
            Package[] packagesArray = adminOps.GetPackages();
            packages = packagesArray != null ? packagesArray.ToList() : new List<Package>();
            string[] packageNames = packages.Select(p => p.Name).ToArray();
            comboBox3.Items.Clear();
            comboBox3.Items.AddRange(packageNames);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0 || comboBox2.SelectedIndex < 0)
            {
                return;
            }
            Package package = new Package()
            {
                SenderId = clients[comboBox2.SelectedIndex].Id,
                ReceiverId = clients[comboBox1.SelectedIndex].Id,
                Name = textBox3.Text,
                Description = textBox4.Text,
                SenderCity = textBox5.Text,
                DestinationCity = textBox6.Text
            };
            adminOps.CreatePackage(package);
            GetPackages();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex < 0)
            {
                return;
            }
            adminOps.RegisterPackage(packages[comboBox3.SelectedIndex].Id);
            GetPackages();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex < 0)
            {
                return;
            }
            if (packages[comboBox3.SelectedIndex].Tracking == false)
            {
                MessageBox.Show("You need to register this package for tracking first!");
                return;
            }
            var time = new MySqlDateTime()
            {
                Year = dateTimePicker1.Value.Year,
                Month = dateTimePicker1.Value.Month,
                Day = dateTimePicker1.Value.Day,
                Hour = dateTimePicker1.Value.Hour,
                Minute = dateTimePicker1.Value.Minute,
                Second = dateTimePicker1.Value.Second,
            };
            PackageUpdate packageUpdate = new PackageUpdate()
            {
                PackageId = packages[comboBox3.SelectedIndex].Id,
                City = textBox7.Text,
                Time = time
            };
            adminOps.CreatePackageUpdate(packageUpdate);
        }
    }
}
