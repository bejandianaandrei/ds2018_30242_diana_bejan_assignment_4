﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using online_tracking_system.AdminServiceReference;
using online_tracking_system.ClientReference;

namespace online_tracking_system
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var adminOps = new AdminOperationsSoapClient();
            var client = adminOps.GetClient(textBox1.Text);
            if (client != null && client.Password == textBox2.Text)
            {
                if (client.IsAdmin)
                {
                    new Form2(adminOps).Show();
                }
                else
                {
                    new Form3(new ServiceClient(), client.Id).Show();
                }
            }
            else
            {
                MessageBox.Show("Invalid username or password");
            }
        }
    }
}
